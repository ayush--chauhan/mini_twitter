class MicropostsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user, only: [:destroy]
  
  def create
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      #Save micropost
      flash[:success] = "Micropost created"
      redirect_to root_url
    else
      #Show error
      @feed_items = current_user.feed.paginate(page: params[:page])
      render 'static_pages/home'
    end
  end
  
  def destroy
    @micropost = Micropost.find(params[:id])
    @micropost.destroy
    flash[:success] = "Micropost deleted"
    redirect_to request.referrer || root_url
  end
  
  private
    def micropost_params
      params.require(:micropost).permit(:content, :picture)
    end
    
    def correct_user
      if current_user != Micropost.find(params[:id]).user
        redirect_to root_url
      end
    end
    
end
