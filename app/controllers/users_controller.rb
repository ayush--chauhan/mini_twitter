class UsersController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update, :index, :destroy]
  before_action :correct_user, only: [:edit, :update]
  before_action :admin_user,     only: :destroy
  
  def new
    @user = User.new
  end
  
  def index
    @users = User.where(activated: true).paginate(page: params[:page])
  end
  
  def edit 
    @user = User.find_by(id: params[:id])
  end
  
  def show 
    @user = User.find(params[:id])
    @microposts = @user.microposts.paginate(page: params[:page])
    if !@user.activated?
      flash[:error] = "Please activate your account to see your profile"
      redirect_to root_url
    end
  end
  
  def create 
    @user = User.new(user_params)
    if @user.save
      flash[:success] = "Please check your email to activate your account"
      @user.send_activation_mail
      redirect_to root_url
    else
      render 'new'
    end
  end
  
  def update 
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      #Update User
      flash[:success] = "User info updated successfully"
      redirect_to @user
    else
      #Show error
      render 'edit'
    end
  end
  
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end
  
  
  def correct_user
    if !current_user?(User.find(params[:id]))
      flash[:danger] = "You dont have authorization to edit this user info"
      redirect_to root_url
    end
  end
  
  def admin_user
    if !current_user.admin?
      redirect_to root_url
    end
  end
  
  private
    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end
end
